package com.docter.web.portlet;

import com.docter.service.model.Docters;
import com.docter.service.service.DoctersLocalService;
import com.docter.service.service.DoctersLocalServiceUtil;
import com.docter.web.constants.DocterWebPortletKeys;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author DELL
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=DocterWeb", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.name=" + DocterWebPortletKeys.DocterPortletName,
		"javax.portlet.resource-bundle=content.Language",
		"com.liferay.portlet.add-default-resource=true",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class DocterWebPortlet extends MVCPortlet {
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		List<Docters> crd = DoctersLocalServiceUtil.getDocterses(-1, -1);
		renderRequest.setAttribute("crd", crd);
		super.render(renderRequest, renderResponse);
	}

	public void editNewsEntry(ActionRequest actionRequest, ActionResponse actionResponse) {
		long docterId = ParamUtil.getLong(actionRequest, "docterId", 0l);
		String docterName = ParamUtil.getString(actionRequest, "docterName");
		ServiceContext serviceContext = new ServiceContext();
		/*
		 * ThemeDisplay themeDisplay = (ThemeDisplay)
		 * actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		 * 
		 * serviceContext.setUserId(themeDisplay.getUserId());
		 * serviceContext.setScopeGroupId(themeDisplay.getScopeGroupId());
		 * serviceContext.setCompanyId(themeDisplay.getCompanyId());
		 */
		// now read your parameters, e.g. like this:
		// long someParameter = ParamUtil.getLong(request, "someParameter");

		try {
			serviceContext = ServiceContextFactory.getInstance(Docters.class.getName(), actionRequest);
			_doctersLocalService.updatedocter(docterId, docterName, serviceContext);
			actionResponse.setRenderParameter("courseId", Long.toString(docterId));
		} catch (SystemException | PortalException e) {
			e.printStackTrace();
		}
	}

	@Reference
	private DoctersLocalService _doctersLocalService;
}