<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.docter.service.model.Docters"%>
<%@page import="java.util.List"%>
<%@ include file="/init.jsp" %>

<portlet:renderURL var="RenderURL">
	<portlet:param name="mvcPath" value="/add.jsp" />
</portlet:renderURL>
<aui:button type="submit" onClick="<%=RenderURL%>" value="add"></aui:button>

<%	
List<Docters> crdlist = (List<Docters>) renderRequest.getAttribute("crd");%>

<liferay-ui:search-container total='<%=crdlist.size()%>'>
	<liferay-ui:search-container-results
		results="<%=ListUtil.subList(crdlist, searchContainer.getStart(), searchContainer.getEnd())%>" />
	<liferay-ui:search-container-row
		className="com.docter.service.model.Docters" modelVar="crd">
		<liferay-ui:search-container-column-text property="docterId"
			name="docterId" />
		<liferay-ui:search-container-column-text property="docterName"
			name="docterName" />
			<liferay-ui:search-container-column-text name="Workflow Status">
				<aui:workflow-status markupView="lexicon" showIcon="<%=false%>"
					showLabel="<%=false%>" status="<%=crd.getStatus()%>" />
			</liferay-ui:search-container-column-text>


		
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator />
</liferay-ui:search-container>